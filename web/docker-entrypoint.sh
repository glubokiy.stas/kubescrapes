#!/bin/bash

while !</dev/tcp/$POSTGRES_HOST/5432; do
    sleep 1;
    echo Waiting for postgres to initialize...
done;

echo "Applying database migrations"
/usr/local/bin/flask db upgrade

if [ "$FLASK_ENV" = "development" ]; then
    echo WORKING IN DEV ENVIRONMENT
    flask run -h 0.0.0.0 -p 8000
else
    echo WORKING IN PROD ENVIRONMENT
    gunicorn -b :8000 $FLASK_APP
fi
