# noinspection PyUnresolvedReferences
from kubescrapes.auth import routes
# noinspection PyUnresolvedReferences
from kubescrapes.cluster import routes
# noinspection PyUnresolvedReferences
from kubescrapes.home import routes
# noinspection PyUnresolvedReferences
from kubescrapes.secret import routes
# noinspection PyUnresolvedReferences
from kubescrapes.spider import routes
# noinspection PyUnresolvedReferences
from kubescrapes.service import routes
# noinspection PyUnresolvedReferences
from kubescrapes.image import routes
