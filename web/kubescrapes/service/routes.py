from flask import render_template, redirect, url_for, Response

from kube.exceptions import KubeException
from kubescrapes.app import app
from kubescrapes.cluster.decorators import cluster_required
from kubescrapes.secret.services import SecretService
from kubescrapes.service.forms import ServiceForm
from kubescrapes.service.services import ServiceService


@app.route('/services')
@cluster_required
def service_list():
    svc_list = ServiceService.list()

    running_builtins = [svc['metadata'].get('labels', {}).get('builtin') for svc in svc_list]
    builtins_to_create = [
        svc_class
        for svc_class in ServiceService.get_svc_classes()
        if svc_class.name not in running_builtins
    ]

    return render_template('service_list.html', services=svc_list, builtins_to_create=builtins_to_create)


@app.route('/services/create', methods=('GET', 'POST'))
@cluster_required
def service_create():
    form = ServiceForm()
    form.secret_name.choices = SecretService.dockerconfig_choices()

    if form.validate_on_submit():
        try:
            ServiceService.save(form)
        except KubeException as e:
            form.non_field_error = str(e)
        else:
            return redirect(url_for('service_list'))

    return render_template('service_create.html', form=form)


@app.route('/services/delete/<string:name>', methods=('POST',))
@cluster_required
def service_delete(name):
    ServiceService.delete(name)
    return redirect(url_for('service_list'))


@app.route('/services/create/<string:name>', methods=('POST',))
@cluster_required
def service_create_builtin(name):
    svc_classes = ServiceService.get_svc_classes()
    svc_class = next((x for x in svc_classes if x.name == name), None)

    if svc_class is None:
        return Response(status=400)

    ServiceService.create_builtin(svc_class)

    return redirect(url_for('service_list'))

