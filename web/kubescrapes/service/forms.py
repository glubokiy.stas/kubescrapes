from flask_wtf import FlaskForm
from wtforms import StringField, validators, IntegerField, SelectField


class ServiceForm(FlaskForm):
    name = StringField(
        'Service Name',
        validators=[validators.length(min=1, max=255), validators.input_required()],
        description='Just name the service as you wish'
    )
    image_name = StringField(
        'Docker Image',
        validators=[validators.length(min=1, max=255), validators.input_required()],
        description='Docker image you want to run'
    )
    port = IntegerField(
        'Port',
        validators=[validators.optional()],
        description='Host port. If not set, will be equal to Service Port'
    )
    service_port = IntegerField(
        'Service Port',
        validators=[validators.input_required()],
        description='Guest port inside a container where the service is run'
    )
    connection_template = StringField(
        'Connection Template',
        validators=[validators.optional(), validators.length(min=1, max=255)],
        description='Connection string with {name} and {port} to be replaced with values. '
                    'Not required, just for showing in table',
        default=r'http://{name}:{port}'
    )
    secret_name = SelectField(
        'Secret Name',
        description='Name of the Secret of type dockerconfigjson. If you do not have one, create it on Secrets page'
    )
