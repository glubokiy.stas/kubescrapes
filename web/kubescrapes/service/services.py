from kubescrapes.cluster.proxy import current_cluster
from kubescrapes.service.forms import ServiceForm


BUILTIN_SERVICES = [
    'Splash'
]


class ServiceService:

    @staticmethod
    def get_svc_classes():
        return [getattr(current_cluster, svc_name) for svc_name in BUILTIN_SERVICES]

    @staticmethod
    def list():
        return current_cluster.Service.get_services()

    @staticmethod
    def save(form: ServiceForm):
        svc = current_cluster.Service(form.name.data)
        port = form.port.data or form.service_port.data
        connection = form.connection_template.data.format(name=svc.name, port=port)
        svc.create(
            image_name=form.image_name.data,
            service_port=int(form.service_port.data),
            port=port,
            annotations={'connection': connection}
        )

    @staticmethod
    def delete(name):
        svc = current_cluster.Service(name)
        svc.delete()

    @staticmethod
    def create_builtin(cls):
        svc = cls()
        svc.create()
