import os

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename


def save_file(file: FileStorage, directory, filename=None):
    if not os.path.exists(directory):
        os.makedirs(directory)

    if filename is None:
        filename = secure_filename(file.filename)

    full_path = os.path.join(directory, filename)
    file.stream.seek(0)
    file.save(full_path)
    return full_path


def save_from_field(field, user_id=0, cluster_name=None, filename=None):
    cluster_name = cluster_name or '_test'

    directory = os.path.expanduser(os.path.join('~/.kubescrapes', str(user_id), cluster_name))

    if not os.path.exists(directory):
        os.makedirs(directory)

    if field.type == 'MultipleFileField':
        paths = []
        for f in field.data:
            paths.append(save_file(f, directory))
        return paths

    if field.type in ['StringField', 'PasswordField', 'TextAreaField']:
        path = os.path.join(directory, filename)

        with open(path, 'w') as f:
            f.write(field.data)

        return path

    file = field.data
    return save_file(file, directory, filename)
