from flask import render_template, redirect, url_for

from kube.exceptions import KubeException
from kubescrapes.app import app
from kubescrapes.cluster.decorators import cluster_required
from kubescrapes.secret.forms import SecretForm
from kubescrapes.secret.services import SecretService


@app.route('/secrets')
@cluster_required
def secret_list():
    return render_template('secret_list.html', secrets=SecretService.list())


@app.route('/secrets/create', methods=('GET', 'POST'))
@cluster_required
def secret_create():
    form = SecretForm()

    if form.validate_on_submit():
        try:
            SecretService.save(form)
        except KubeException as e:
            form.non_field_error = str(e)
        else:
            return redirect(url_for('secret_list'))

    return render_template('secret_create.html', form=form)


@app.route('/secrets/delete/<string:name>', methods=('POST',))
@cluster_required
def secret_delete(name):
    SecretService.delete(name)
    return redirect(url_for('secret_list'))
