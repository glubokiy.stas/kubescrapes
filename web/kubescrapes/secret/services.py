from kubescrapes.cluster.proxy import current_cluster


class SecretService:

    @staticmethod
    def list():
        return current_cluster.DockerSecret.get_secrets()

    @staticmethod
    def save(form):
        secret = current_cluster.DockerSecret(form.name.data)

        secret.create(
            registry_server=form.server.data,
            username=form.username.data,
            password=form.password.data,
            email=form.email.data,
        )

        return secret

    @staticmethod
    def delete(name):
        secret = current_cluster.DockerSecret(name)
        secret.delete()

    @staticmethod
    def dockerconfig_choices():
        secret_names = [
            s['metadata']['name']
            for s in SecretService.list()
            if s['type'] == 'kubernetes.io/dockerconfigjson'
        ]
        return [(s, s) for s in secret_names]
