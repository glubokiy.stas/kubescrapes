from flask_wtf import FlaskForm
from wtforms import StringField, validators, PasswordField


class SecretForm(FlaskForm):
    server = StringField(
        'Registry Server',
        validators=[validators.length(min=1, max=255), validators.input_required()],
        description='Private Docker Registry FQDN (https://index.docker.io/v1/ for DockerHub)'
    )
    name = StringField(
        'Secret Name',
        validators=[validators.length(min=1, max=255), validators.input_required()],
        description='Just name the secret as you wish'
    )
    email = StringField(
        'Registry Account Email',
        validators=[validators.length(min=1, max=255), validators.input_required()],
        description='Email for your Docker registry'
    )
    username = StringField(
        'Registry Account Username',
        validators=[validators.length(min=1, max=255), validators.input_required()],
        description='Username for your Docker registry'
    )
    password = PasswordField(
        'Registry Account Password',
        validators=[validators.length(min=1, max=255), validators.input_required()],
        description='Password for your Docker registry'
    )
