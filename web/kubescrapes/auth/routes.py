from flask import redirect, url_for, flash, session
from flask_dance import OAuth2ConsumerBlueprint
from flask_dance.consumer import oauth_authorized, oauth_before_login
from flask_dance.consumer.storage.sqla import SQLAlchemyStorage
from flask_dance.contrib.google import make_google_blueprint
from flask_login import current_user, LoginManager, login_required, logout_user, login_user
from sqlalchemy.orm.exc import NoResultFound

from kubescrapes.app import app
from kubescrapes.auth.models import User, OAuth
from kubescrapes.db import db


# def login(self):
#     self.session.redirect_uri = url_for(".authorized", _external=True, _scheme=app.config.get('PREFERRED_URL_SCHEME'))
#     app.logger.info(f'Redirect uri: {self.session.redirect_uri}')
#     url, state = self.session.authorization_url(
#         self.authorization_url, state=self.state, **self.authorization_url_params
#     )
#     state_key = "{bp.name}_oauth_state".format(bp=self)
#     session[state_key] = state
#     oauth_before_login.send(self, url=url)
#     app.logger.info(f'Final url: {url}')
#     return redirect(url)
#
#
# # Monkeypatch login method
# OAuth2ConsumerBlueprint.login = login


google_bp = make_google_blueprint(
    storage=SQLAlchemyStorage(OAuth, db.session, user=current_user),
    client_id='347225167034-d35qe6mkbeqg3qv14hir0qi8li14jbv5.apps.googleusercontent.com',
    client_secret='_VcG9EwjpQ54jm-QalybURUX',
    scope=['profile', 'email'],
    redirect_to='home'
)


app.register_blueprint(google_bp, url_prefix='/login')

# setup login manager
login_manager = LoginManager()
login_manager.login_view = 'google.login'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@login_manager.unauthorized_handler
def unauthorized():
    flash('You are not authenticated', category='dialog_warning')
    return redirect(url_for('home'))


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


# noinspection PyArgumentList
@oauth_authorized.connect_via(google_bp)
def google_logged_in(blueprint, token):
    if not token:
        app.logger.warn('Failed to log')
        flash('Failed to log in with Google', category='dialog_error')
        return False

    resp = blueprint.session.get('/oauth2/v1/userinfo')
    if not resp.ok:
        app.logger.warn(f'Failed to fetch user: {resp}')
        flash('Failed to fetch user info from Google', category='dialog_error')
        return False

    google_info = resp.json()

    app.logger.info(f'Fetched google info: {google_info}')

    # Find this OAuth token in the database, or create it
    query = OAuth.query.filter_by(
        provider=blueprint.name,
        google_id=google_info['id'],
    )

    try:
        oauth = query.one()
    except NoResultFound:
        oauth = OAuth(
            provider=blueprint.name,
            google_id=google_info['id'],
            token=token,
        )

    if not oauth.user:
        # If this OAuth token doesn't have an associated local account,
        # create a new local user account for this user. We can log
        # in that account as well, while we're at it.
        user = User(
            email=google_info['email'],
            name=google_info['name']
        )
        oauth.user = user

        db.session.add_all([user, oauth])
        db.session.commit()

    login_user(oauth.user)
    flash('Successfully signed in with Google.')

    # Since we're manually creating the OAuth model in the database,
    # we should return False so that Flask-Dance knows that
    # it doesn't have to do it. If we don't return False, the OAuth token
    # could be saved twice, or Flask-Dance could throw an error when
    # trying to incorrectly save it for us.
    return False
