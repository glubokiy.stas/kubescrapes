from flask_dance.consumer.storage.sqla import OAuthConsumerMixin
from flask_login import UserMixin

from kubescrapes.db import db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(db.String(256), unique=True)
    name = db.Column(db.String(256))


class OAuth(db.Model, OAuthConsumerMixin):
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)

    google_id = db.Column(db.String(128), unique=True)
