import logging
from os import getenv

from flask import Flask

app = Flask(__name__, template_folder='_templates', static_url_path='/static', static_folder='_static')
app.secret_key = 'A0Zr98j/3yX HHs!jd2mN]acX/,?RT'
app.config['TEMPLATES_AUTO_RELOAD'] = True

if getenv('FLASK_ENV') == 'production':
    app.config['PREFERRED_URL_SCHEME'] = 'https'

if getenv('FLASK_ENV') != 'development':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

# noinspection PyUnresolvedReferences
import kubescrapes.db
# noinspection PyUnresolvedReferences
import kubescrapes.models
# noinspection PyUnresolvedReferences
import kubescrapes.routes
# noinspection PyUnresolvedReferences
import kubescrapes.filters
