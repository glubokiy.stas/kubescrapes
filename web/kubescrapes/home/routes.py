from flask import render_template

from kubescrapes.app import app


@app.route('/')
def home():
    return render_template('home.html')
