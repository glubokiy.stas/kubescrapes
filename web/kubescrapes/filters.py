from datetime import datetime

from kubescrapes.app import app


@app.template_filter('pluralize')
def pluralize(number, singular='', plural='s'):
    if number == 1:
        return singular
    else:
        return plural


@app.template_filter('k8s_time')
def k8s_time(time_string):
    return datetime.strptime(time_string, '%Y-%m-%dT%H:%M:%SZ').strftime('%d %b %Y %H:%M')
