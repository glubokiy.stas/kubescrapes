from docker.errors import APIError
from flask import render_template, redirect, url_for, flash

from kubescrapes.app import app
from kubescrapes.cluster.decorators import cluster_required
from kubescrapes.image.services import ImageService


@app.route('/images')
@cluster_required
def image_list():
    image_service = ImageService()

    if not image_service.cluster.docker_host:
        context = {'docker_enabled': False}
    else:
        context = {
            'docker_enabled': True,
            'images': image_service.list()
        }

    return render_template('image_list.html', **context)


@app.route('/images/delete/<path:name>', methods=('POST',))
@cluster_required
def image_delete(name):
    image_service = ImageService()

    try:
        image_service.delete(name)
    except APIError as e:
        flash(str(e), 'dialog_error')

    return redirect(url_for('image_list'))
