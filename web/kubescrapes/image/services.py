from kube.docker.client import get_docker_client
from kubescrapes.cluster.services import ClusterService


class ImageService:

    def __init__(self):
        self.cluster = ClusterService.get_active()
        self.client = get_docker_client(
            host=self.cluster.docker_host,
            cert_path=self.cluster.docker_cert_path,
            api_version=self.cluster.docker_api_version
        )

    def list(self):
        return [i for i in self.client.images.list() if i.tags and 'gcr.io' not in i.tags[0]]

    def delete(self, name):
        self.client.images.remove(name)
