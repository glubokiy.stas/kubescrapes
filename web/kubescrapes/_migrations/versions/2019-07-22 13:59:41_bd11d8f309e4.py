"""empty message

Revision ID: bd11d8f309e4
Revises: c4317a9ec03d
Create Date: 2019-07-22 13:59:41.655626

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bd11d8f309e4'
down_revision = 'c4317a9ec03d'
branch_labels = None
depends_on = None


def upgrade():
    op.execute('ALTER TABLE cluster DROP CONSTRAINT cluster_pkey CASCADE')

    op.add_column('cluster', sa.Column('id', sa.Integer(), nullable=False, server_default='1'))
    op.add_column('spider', sa.Column('cluster_id', sa.Integer(), nullable=False, server_default='1'))

    op.execute('UPDATE cluster SET id = (SELECT row_number() over () FROM cluster)')
    op.execute(
        'UPDATE spider SET cluster_id = (SELECT cluster.id FROM cluster WHERE cluster.name = spider.cluster_name)'
    )

    op.execute('''
        create sequence cluster_id_seq;
        alter table cluster alter column id set default nextval('public.cluster_id_seq');
        alter sequence cluster_id_seq owned by cluster.id;
        select setval('cluster_id_seq', COALESCE((SELECT MAX(id)+1 FROM cluster), 1), false);
    ''')

    op.create_primary_key('cluster_pkey', 'cluster', ['id'])
    op.create_foreign_key('spider_cluster_id_fkey', 'spider', 'cluster', ['cluster_id'], ['id'], ondelete='CASCADE')

    op.drop_column('spider', 'cluster_name')


def downgrade():
    op.add_column('spider', sa.Column('cluster_name', sa.VARCHAR(length=80), autoincrement=False, nullable=False, server_default='1'))

    op.execute('ALTER TABLE cluster DROP CONSTRAINT cluster_pkey CASCADE')

    op.execute(
        'UPDATE spider SET cluster_name = (SELECT cluster.name FROM cluster WHERE cluster.id = spider.cluster_id)'
    )

    op.drop_column('spider', 'cluster_id')
    op.drop_column('cluster', 'id')

    op.create_primary_key('cluster_pkey', 'cluster', ['name'])
    op.create_foreign_key('spider_cluster_name_fkey', 'spider', 'cluster', ['cluster_name'], ['name'], ondelete='CASCADE')
