from kubescrapes.db import db


class Spider(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    name = db.Column(db.String(80), nullable=False)
    image_name = db.Column(db.String(80), nullable=False)
    env_vars = db.Column(db.Text(), nullable=False)
    secret_name = db.Column(db.String(80), nullable=True)
    num_jobs_run = db.Column(db.Integer, server_default='0', default=0)

    cluster_id = db.Column(db.Integer, db.ForeignKey('cluster.id', ondelete='CASCADE'), nullable=False)
    cluster = db.relationship('Cluster', backref=db.backref('spider', passive_deletes=True))

    def __repr__(self):
        return '<Spider %r>' % self.name

