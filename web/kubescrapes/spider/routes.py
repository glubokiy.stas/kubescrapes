import re

from flask import render_template, redirect, url_for, request, flash
from pykube import CronJob

from kube.utils.pod_logs import pod_logs
from kubescrapes.app import app
from kubescrapes.cluster.decorators import cluster_required
from kubescrapes.secret.services import SecretService
from kubescrapes.spider.forms import SpiderForm, CrawlForm
from kubescrapes.spider.services import SpiderService

CronJob.version = 'batch/v1beta1'


@app.route('/spiders')
@cluster_required
def spider_list():
    return render_template('spider_list.html', spiders=SpiderService.list())


@app.route('/spiders/create', methods=('GET', 'POST'))
@cluster_required
def spider_create():
    form = SpiderForm()
    form.secret_name.choices = SecretService.dockerconfig_choices()

    if form.validate_on_submit():
        SpiderService.save(form)
        return redirect(url_for('spider_list'))

    return render_template(
        'spider_form.html',
        form=form,
        action='Create',
        url=url_for('spider_create')
    )


@app.route('/spiders/<string:name>/update', methods=('GET', 'POST'))
@cluster_required
def spider_update(name):
    spider = SpiderService.get(name)

    if request.method == 'GET':
        form = SpiderForm(obj=spider)
    else:
        form = SpiderForm()

    form.secret_name.choices = SecretService.dockerconfig_choices()

    if form.validate_on_submit():
        SpiderService.update(form, spider)
        return redirect(url_for('spider_list'))

    return render_template(
        'spider_form.html',
        form=form,
        action='Update',
        url=url_for('spider_update', name=spider.name)
    )


@app.route('/spiders/<string:name>/delete', methods=('POST',))
@cluster_required
def spider_delete(name):
    SpiderService.delete(name)
    return redirect(url_for('spider_list'))


@app.route('/spiders/<string:name>/crawl', methods=('GET', 'POST'))
@cluster_required
def spider_crawl(name):
    spider = SpiderService.get(name)
    default_job_name = SpiderService.get_default_job_name(spider)

    form = CrawlForm()
    if request.method == 'GET':
        form.job_name.default = default_job_name
        form.process()

    if form.validate_on_submit():
        SpiderService.crawl(form, spider)
        return redirect(url_for('spider_detail', name=spider.name))

    return render_template(
        'spider_crawl_form.html',
        form=form,
        spider=spider,
        default_job_name=default_job_name
    )


@app.route('/spiders/<string:name>')
@cluster_required
def spider_detail(name):
    spider = SpiderService.get(name)
    spider_jobs = SpiderService.get_spider_jobs(spider.name)
    spider_cronjobs = SpiderService.get_spider_cronjobs(spider.name)

    return render_template(
        'spider_detail.html',
        spider=spider,
        spider_jobs=spider_jobs,
        spider_cronjobs=spider_cronjobs
    )


@app.route('/spider-jobs/<string:name>')
@cluster_required
def spider_job_detail(name):
    spider_job = SpiderService.get_job(name)
    pods = SpiderService.get_pods(name)

    return render_template(
        'spider_job_detail.html',
        job=spider_job,
        pods=pods
    )


@app.route('/spider-pods/<string:name>')
@cluster_required
def spider_pod_detail(name):
    max_logs = 25
    spider_pod = SpiderService.get_pod(name)

    logs = pod_logs(spider_pod)
    lines = logs.split('\n')
    has_more_logs = len(lines) > max_logs
    cut_logs = '\n'.join(lines[-max_logs:])

    search_for_stats = re.search(r'Dumping Scrapy stats:\n\{([^\}]+)\}', logs, re.M)
    if search_for_stats is not None:
        stats_string = search_for_stats.group(1)
        stats = re.findall(r'\'([^\']+)\': (\d+)', stats_string)
    else:
        stats = None

    return render_template(
        'spider_pod_detail.html',
        pod=spider_pod,
        logs=cut_logs,
        has_more_logs=has_more_logs,
        stats=stats
    )


@app.route('/spider-pods/<string:name>/logs')
@cluster_required
def spider_pod_logs(name):
    spider_pod = SpiderService.get_pod(name)
    logs = pod_logs(spider_pod)
    return logs


@app.route('/spider-jobs/<string:name>/delete', methods=('POST',))
@cluster_required
def spider_job_delete(name):
    SpiderService.get_job(name).delete()
    return redirect(request.referrer or url_for('spider_list'))


@app.route('/spider-cron-jobs/<string:name>/delete', methods=('POST',))
@cluster_required
def spider_cronjob_delete(name):
    SpiderService.get_cronjob(name).delete()
    return redirect(request.referrer or url_for('spider_list'))
