import re

from flask import abort as _abort

from kube.spider.spider import KubeSpider
from kubescrapes.cluster.proxy import current_cluster
from kubescrapes.db import db
from kubescrapes.spider.forms import SpiderForm, CrawlForm
from kubescrapes.spider.models import Spider


class SpiderService:

    @classmethod
    def list(cls):
        return Spider.query.filter_by(cluster_id=current_cluster.id).all()

    @classmethod
    def get(cls, name, abort=True):
        spider = Spider.query.filter_by(cluster_id=current_cluster.id, name=name).first()

        if spider is None and abort:
            _abort(404)

        return spider

    @classmethod
    def save(cls, form: SpiderForm):
        spider = Spider(
            name=form.name.data,
            image_name=form.image_name.data,
            env_vars=form.env_vars.data,
            secret_name=form.secret_name.data,
            cluster_id=current_cluster.id
        )

        db.session.add(spider)
        db.session.commit()

    @classmethod
    def update(cls, form: SpiderForm, obj: Spider):
        form.populate_obj(obj)

        db.session.commit()

    @classmethod
    def delete(cls, name):
        spider = Spider.query.filter_by(name=name).first()

        db.session.delete(spider)
        db.session.commit()

    @classmethod
    def crawl(cls, form: CrawlForm, spider):
        schedule = form.schedule.data if form.schedule.data else None

        kube_spider: KubeSpider = current_cluster.Spider(
            name=spider.name,
            job_name=form.job_name.data,
            image_name=spider.image_name,
            env=cls.parse_env_vars(spider.env_vars + '\n' + form.env_vars.data),
            registry_secret_name=spider.secret_name,
            command=form.command.data,
            schedule=schedule
        )

        kube_spider.create()

        spider.num_jobs_run += 1
        db.session.commit()

    @classmethod
    def parse_env_vars(cls, env_vars):
        secret_pattern = re.compile(r'([^\d=][^=]*)=\{secret:(.+)\.([^\.]+)\}')
        simple_pattern = re.compile(r'([^\d=][^=]*)=(.*)')

        lines = [line for line in env_vars.splitlines() if line]

        env = []
        for line in lines:
            match = re.match(secret_pattern, line)
            if match is not None:
                name, secret, key = match.groups()
                env.append({
                    'name': name,
                    'valueFrom': {
                        'secretKeyRef': {
                            'name': secret,
                            'key': key
                        }
                    }
                })
                continue

            match = re.match(simple_pattern, line)
            if match is not None:
                name, value = match.groups()
                env.append({
                    'name': name,
                    'value': value
                })
                continue

            raise ValueError(f'Invalid env var declaration: {line}')

        return env

    @classmethod
    def get_default_job_name(cls, spider):
        return f'{spider.name}-run-{spider.num_jobs_run+1}'

    @classmethod
    def get_spider_jobs(cls, spider_name):
        return current_cluster.Spider.get_jobs(f'spider={spider_name}')

    @classmethod
    def get_spider_cronjobs(cls, spider_name):
        return current_cluster.Spider.get_cronjobs(f'spider={spider_name}')

    @classmethod
    def get_job(cls, job_name, abort=True):
        spider_job = current_cluster.Spider.get_job(name=job_name)

        if spider_job is None and abort:
            _abort(404)

        return spider_job

    @classmethod
    def get_cronjob(cls, job_name, abort=True):
        spider_job = current_cluster.Spider.get_cronjob(name=job_name)

        if spider_job is None and abort:
            _abort(404)

        return spider_job

    @classmethod
    def get_pods(cls, job_name):
        return current_cluster.Spider.get_pods(f'job-name={job_name}')

    @classmethod
    def get_pod(cls, pod_name):
        return current_cluster.Spider.get_pod(name=pod_name)
