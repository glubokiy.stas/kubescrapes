from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, validators, SelectField, TextAreaField, ValidationError


def validate_env_vars(_, field):
    # Need a local import to avoid circular import
    from kubescrapes.spider.services import SpiderService

    try:
        SpiderService.parse_env_vars(field.data)
    except ValueError as err:
        raise ValidationError(str(err))


class SpiderForm(FlaskForm):
    name = StringField(
        'Spider Name',
        validators=[validators.length(min=1, max=80)],
        description='Name of the spider'
    )
    image_name = StringField(
        'Image Name',
        validators=[validators.length(min=1, max=80)],
        description='Docker image you want to run'
    )
    env_vars = TextAreaField(
        'Environment Variables',
        validators=[validate_env_vars],
        description=(
            'List of environment variables to use in every crawl (in `NAME=value` format). '
            'Format for using secret value: `NAME={secret:secret_name.key}`'
        )
    )
    secret_name = SelectField(
        'Secret Name',
        validators=[validators.optional()],
        coerce=lambda x: x if x else None,
        description='Name of the Secret of type dockerconfigjson. If you do not have one, create it on Secrets page'
    )

    def validate_name(self, field):
        if 'update' in str(request.url_rule):
            return

        from kubescrapes.spider.services import SpiderService
        spider = SpiderService.get(field.data, abort=False)

        if spider is not None:
            raise ValidationError('Spider with this name already exists')


class CrawlForm(FlaskForm):
    job_name = StringField(
        'Job Name',
        validators=[validators.length(min=1, max=80)],
        description='Name of the job to be run'
    )
    env_vars = TextAreaField(
        'Environment Variables',
        validators=[validate_env_vars],
        description='List of environment variables to use in this crawl'
    )
    command = StringField(
        'Command',
        description='Command to run. If left blank, default CMD is used',
        default='scrapy crawl $(SPIDER_NAME)'
    )
    schedule = StringField(
        'Schedule',
        validators=[validators.optional()],
        description='Schedule in crontab format (`*/5 * * * *` for every 5 minutes)'
    )
