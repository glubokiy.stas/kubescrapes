from flask_wtf import FlaskForm
from wtforms import StringField, validators, TextAreaField, PasswordField

from kube.base.cluster import KubeCluster
from kubescrapes.utils import save_from_field


class ClusterForm(FlaskForm):
    name = StringField(
        'Cluster Name',
        validators=[validators.length(min=1, max=80), validators.input_required()],
        description='Just name the cluster as you wish'
    )
    server = StringField(
        'Server API URL',
        validators=[validators.length(min=5, max=80), validators.input_required()],
        description='URL that Kubernetes uses to access the Kubernetes API'
    )
    service_token = PasswordField(
        'Service Token',
        validators=[validators.input_required()],
        description='Service account token for cluster namespace authentication'
    )
    namespace = StringField(
        'Project Namespace',
        default='kubescrapes',
        description='Namespace in which spiders and services will be deployed'
    )
    ca_crt = TextAreaField(
        'CA Certificate',
        validators=[validators.input_required()],
        description='Valid Kubernetes certificate for cluster authentication'
    )

    def validate(self):
        is_valid = super().validate()

        if not is_valid:
            return is_valid

        server = self.server.data
        ca_crt_path = save_from_field(self.ca_crt, None, None, 'ca.crt')
        token = self.service_token.data

        cluster = KubeCluster(0, 'test', server, ca_crt_path, token, self.namespace.data)
        is_cluster_connected = cluster.ping()

        if not is_cluster_connected:
            self.non_field_error = 'Cannot connect to cluster'
            return is_cluster_connected

        return is_cluster_connected
