from flask_login import current_user
from werkzeug.local import LocalProxy

from kubescrapes.cluster.models import Cluster
from kubescrapes.cluster.utils import get_kubecluster

current_cluster = LocalProxy(lambda: get_current_cluster())


def get_current_cluster():
    if not current_user.is_authenticated:
        return

    cluster = Cluster.query.filter(Cluster.user == current_user, Cluster.is_active).first()

    if cluster is None:
        return

    return get_kubecluster(cluster)
