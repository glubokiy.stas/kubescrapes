from flask import render_template, redirect, url_for, session, Response
from flask_login import login_required

from kubescrapes.app import app
from kubescrapes.cluster.decorators import cluster_required
from kubescrapes.cluster.forms import ClusterForm
from kubescrapes.cluster.services import ClusterService


@app.route('/clusters/connect', methods=('GET', 'POST'))
@login_required
def cluster_create():
    message = session.pop('message', None)
    form = ClusterForm()

    if form.validate_on_submit():
        ClusterService.save(form)

        return redirect(url_for('cluster_list'))

    return render_template('cluster_create.html', message=message, form=form)


@app.route('/clusters')
@login_required
def cluster_list():
    return render_template('cluster_list.html', clusters=ClusterService.list())


@app.route('/clusters/set-current/<string:name>', methods=('POST',))
@cluster_required
def cluster_set_current(name):
    ClusterService.set_active(name)
    return Response()


@app.route('/clusters/delete/<string:name>', methods=('POST',))
@cluster_required
def cluster_delete(name):
    ClusterService.delete(name)
    return redirect(url_for('cluster_list'))
