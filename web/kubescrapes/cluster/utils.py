from kube.base.cluster import KubeCluster


def get_kubecluster(cluster):
    return KubeCluster(
        cluster.id,
        cluster.name,
        cluster.server,
        cluster.ca_crt,
        cluster.service_token,
        cluster.namespace
    )
