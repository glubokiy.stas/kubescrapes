from functools import wraps

from flask import redirect, url_for, flash
from flask_login import current_user, login_required


def cluster_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.clusters:
            flash('To start using Kubernetes, connect to cluster', 'dialog_warning')
            return redirect(url_for('cluster_create'))

        return func(*args, **kwargs)

    decorated_view = login_required(decorated_view)

    return decorated_view
