from flask import current_app
from flask_login import current_user
from pykube import Namespace

from kube.base.cluster import KubeCluster
from kubescrapes.cluster.forms import ClusterForm
from kubescrapes.cluster.models import Cluster
from kubescrapes.cluster.proxy import current_cluster, get_current_cluster
from kubescrapes.cluster.utils import get_kubecluster
from kubescrapes.db import db
from kubescrapes.utils import save_from_field


class ClusterService:

    @staticmethod
    def get_active():
        return Cluster.query.filter_by(is_active=True).first()

    @staticmethod
    def set_active(name):
        Cluster.query.update({Cluster.is_active: False})
        cluster = Cluster.query.filter_by(name=name).first()
        cluster.is_active = True

        db.session.commit()

    @staticmethod
    def save(form: ClusterForm):
        ca_crt_path = save_from_field(
            field=form.ca_crt,
            user_id=current_user.id,
            cluster_name=form.name.data,
            filename='ca.crt'
        )

        data = {
            'name': form.name.data,
            'server': form.server.data,
            'ca_crt': ca_crt_path,
            'service_token': form.service_token.data,
            'namespace': form.namespace.data,
            'user': current_user
        }

        cluster = ClusterService.create(**data)

        ClusterService.set_active(cluster.name)

        return cluster

    @staticmethod
    def create(**kwargs):
        cluster = Cluster(**kwargs)

        db.session.add(cluster)
        db.session.commit()

        kubecluster = get_kubecluster(cluster)
        ns = kubecluster.Namespace(cluster.namespace)

        if not ns.exists():
            ns.create()

        return cluster

    @staticmethod
    def list():
        return Cluster.query.all()

    @staticmethod
    def delete(name):
        cluster = Cluster.query.filter_by(name=name).first()
        was_active = cluster.is_active

        db.session.delete(cluster)

        if was_active:
            c = Cluster.query.first()
            if c:
                c.is_active = True

        db.session.commit()
