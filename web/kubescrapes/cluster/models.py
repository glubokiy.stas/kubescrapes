from kubescrapes.db import db


class Cluster(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(80), nullable=False)
    server = db.Column(db.String(80), nullable=False)
    service_token = db.Column(db.String(900), nullable=False)
    ca_crt = db.Column(db.String(80), nullable=False)
    namespace = db.Column(db.String(80), nullable=False, server_default='kubescrapes')
    is_active = db.Column(db.Boolean())

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('clusters'))

    def __repr__(self):
        return '<Cluster %r>' % self.name
