import os
from os.path import join, dirname

from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from kubescrapes.app import app

POSTGRES = {
    'user': os.getenv('POSTGRES_USER', 'postgres'),
    'pw': os.getenv('POSTGRES_PASSWORD', 'docker'),
    'db': os.getenv('POSTGRES_DB', 'kubescrapes'),
    'host': os.getenv('POSTGRES_HOST', 'localhost'),
    'port': os.getenv('POSTGRES_PORT', '5432'),
}

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{user}:{pw}@{host}:{port}/{db}'.format(**POSTGRES)

db = SQLAlchemy(app)
migrate = Migrate(app, db, directory=join(dirname(__file__), '_migrations'))
db.init_app(app)
