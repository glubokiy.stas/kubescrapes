class KubeException(Exception):
    pass


class KubeConnectionError(KubeException):
    pass
