from kube.service.base import BuiltinService


class KubeMongoDB(BuiltinService):
    name = 'mongodb'
    image_name = 'mongo'
    service_port = 27017

    @property
    def connection(self):
        return f'mongodb://{self.name}:{self.port}'
