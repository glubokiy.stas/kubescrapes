from kube.service.base import BuiltinService


class KubeSplash(BuiltinService):
    name = 'splash'
    image_name = 'scrapinghub/splash'
    service_port = 8050

    @property
    def connection(self):
        return f'http://{self.name}:{self.port}'
