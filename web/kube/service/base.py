from kube.base.service import KubeService


class BuiltinService(KubeService):
    name = None
    image_name = None
    service_port = None
    connection = None

    def __init__(self, name=None, *args, **kwargs):
        if name is None:
            name = self.name

        super().__init__(name, *args, **kwargs)
        self.port = type(self).service_port

    def create(self, **kwargs):
        super().create(
            image_name=type(self).image_name,
            service_port=type(self).service_port,
            extra_labels=self._extra_labels,
            annotations=self._annotations
        )

    @property
    def _extra_labels(self):
        return {
            'builtin': type(self).name
        }

    @property
    def _annotations(self):
        return {
            'connection': self.connection
        }
