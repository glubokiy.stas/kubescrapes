import docker
from docker import DockerClient


def get_docker_client(host, cert_path, tls_verify='1', api_version='1.35') -> DockerClient:
    # docker_env = {
    #     'DOCKER_TLS_VERIFY': '1',
    #     'DOCKER_HOST': 'tcp://192.168.99.100:2376',
    #     'DOCKER_CERT_PATH': '/.minikube/certs',
    #     'DOCKER_API_VERSION': '1.35'
    # }

    docker_env = {
        'DOCKER_TLS_VERIFY': tls_verify,
        'DOCKER_HOST': host,
        'DOCKER_CERT_PATH': cert_path,
        'DOCKER_API_VERSION': api_version
    }

    return docker.from_env(version=docker_env['DOCKER_API_VERSION'], environment=docker_env)


def docker_ping(host, cert_path, api_version):
    client = get_docker_client(host, cert_path, '1', api_version)
    try:
        client.images.list()
    except Exception:
        success = False
    else:
        success = True

    return success
