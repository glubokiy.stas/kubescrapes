import logging
from os.path import relpath
from tempfile import NamedTemporaryFile

from docker.errors import ImageNotFound
from docker.models.images import Image

from kube.docker.client import get_docker_client

logger = logging.getLogger(__name__)


class KubeImage:

    def __init__(self, name: str, docker_kwargs: dict):
        self.name = name

        self.client = get_docker_client(**docker_kwargs)

    def build(
            self,
            path: str,
            requirements_path: str=None,
            python_version: str='latest',
            dockerfile_content: str=None
    ) -> Image:
        if requirements_path is not None:
            rel_requirements_path = relpath(requirements_path, path)
        else:
            rel_requirements_path = 'requirements.txt'

        with NamedTemporaryFile() as dockerfile:
            if dockerfile_content is None:
                dockerfile_content = (
                    f'FROM python:{python_version}\n'
                    f'ADD . /usr/src/app\n'
                    f'WORKDIR /usr/src/app\n'
                    f'RUN pip install -r {rel_requirements_path}\n'
                    f'CMD scrapy crawl $SPIDER_NAME\n'
                )
            dockerfile.write(dockerfile_content.encode('utf-8'))

            dockerfile.seek(0)

            logger.info(f'Building image {self.name}...')
            image, _ = self.client.images.build(
                tag=self.name,
                path=path,
                dockerfile=dockerfile.name
            )

        return image

    def exists(self) -> bool:
        try:
            self.client.images.get(self.name)
            return True
        except ImageNotFound:
            return False

    def remove(self):
        self.client.images.remove(self.name)

    def __repr__(self):
        return f'<KubeImage "{self.name}">'
