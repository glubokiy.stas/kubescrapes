from pykube import Namespace

from kube.base.object import KubeObject


class KubeNamespace(KubeObject):

    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.name = name

    @property
    def namespace_obj(self):
        return {
            'apiVersion': 'v1',
            'kind': 'Namespace',
            'metadata': {
                'name': self.name,
            }
        }

    @property
    def namespace(self):
        return Namespace(self.api, self.namespace_obj)

    def create(self):
        self.namespace.create()

    def exists(self):
        return self.namespace.exists()

    def delete(self):
        return self.namespace.delete()

    @classmethod
    def get_namespaces(cls):
        api = cls.api
        return Namespace.objects(api).response['items']
