from glob import glob
from importlib import import_module
from inspect import isclass
from os.path import dirname, join, relpath

from pykube import KubeConfig, HTTPClient, Service

from kube.base.object import KubeObject


def get_objects():
    kube_module_names = [relpath(x).replace('/', '.')[:-3] for x in glob(join(dirname(__file__), '..', '**', '*.py'))]

    objects = {}
    for module_name in kube_module_names:
        module = import_module(module_name)
        for obj in vars(module).values():
            if isclass(obj) and issubclass(obj, KubeObject) and obj not in objects:
                name = obj.__name__
                if name.startswith('Kube'):
                    name = name[4:]
                objects[name] = obj

    return objects


def get_class_with_api(cls, api):
    return type(cls.__name__, (cls,), {'api': api})


class KubeCluster:

    def __init__(self, id_, name, server, ca_crt_path, token, namespace='kubescrapes'):
        self.id = id_
        self.name = name
        self.server = server
        self.ca_crt_path = ca_crt_path
        self.token = token
        self.namespace = namespace

        for obj_name, obj_class in get_objects().items():
            setattr(self, obj_name, get_class_with_api(obj_class, self.api))

    @property
    def doc(self):
        return {
            'clusters': [
                {
                    'name': self.name,
                    'cluster': {
                        'server': self.server,
                        'certificate-authority': self.ca_crt_path,
                    },
                },
            ],
            'users': [
                {
                    'name': self.name,
                    'user': {
                        'token': self.token,
                    },
                },
            ],
            'contexts': [
                {
                    'name': self.name,
                    'context': {
                        'cluster': self.name,
                        'user': self.name,
                        'namespace': self.namespace
                    },
                }
            ],
            'current-context': self.name,
        }

    @property
    def config(self):
        return KubeConfig(self.doc)

    @property
    def api(self):
        return HTTPClient(self.config)

    def ping(self):
        try:
            Service.objects(self.api).response['items']
        except Exception:
            success = False
        else:
            success = True

        return success
