class KubeObject:

    api = None
    name = None

    def __init__(self, api=None):
        if api:
            self.api = api

        if not self.api:
            raise ValueError('api is not set')

    def __repr__(self):
        return f'<{self.__class__.__name__} "{self.name}">'
