import base64
import json
import logging

import requests
from pykube import Secret

from kube.base.object import KubeObject
from kube.exceptions import KubeConnectionError

logger = logging.getLogger(__name__)


class KubeDockerSecret(KubeObject):

    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.name = name
        self.registry_server = None
        self.username = None
        self.password = None
        self.email = None

    @property
    def secret_obj(self):
        return {
            'apiVersion': 'v1',
            'data': {
                '.dockerconfigjson': self.dockerconfigjson
            },
            'kind': 'Secret',
            'metadata': {
                'name': self.name,
            },
            'type': 'kubernetes.io/dockerconfigjson',
        }

    @property
    def secret(self):
        return Secret(self.api, self.secret_obj)

    @property
    def dockerconfigjson(self):
        auth_str = f'{self.username}:{self.password}'
        auth = base64.b64encode(bytes(auth_str, 'utf-8')).decode('utf-8')

        data = {
            'auths': {
                self.registry_server: {
                    'username': self.username,
                    'password': self.password,
                    'email': self.email,
                    'auth': auth,
                }
            }
        }

        return base64.b64encode(bytes(json.dumps(data), 'utf-8')).decode('utf-8')

    def create(self, registry_server, username, password, email):
        self.registry_server = registry_server
        self.username = username
        self.password = password
        self.email = email

        try:
            logger.info(f'Creating secret {self.name}')
            self.secret.create()
        except requests.exceptions.ConnectionError:
            raise KubeConnectionError('Cannot connect to cluster')

    def delete(self):
        self.secret.delete()

    def exists(self):
        self.secret.exists()

    @classmethod
    def get_secrets(cls):
        api = cls.api
        return Secret.objects(api).response['items']
