import logging
from time import sleep

import requests
from pykube import Service, Deployment, Pod, ReplicaSet

from kube.base.object import KubeObject
from kube.exceptions import KubeConnectionError, KubeException

logger = logging.getLogger(__name__)


class KubeService(KubeObject):

    def __init__(self, name, api=None):
        super().__init__(api)

        self.name = name

        self.service_port = None
        self.image_name = None
        self.port = None
        self.annotations = {}
        self.extra_labels = {}
        self.secret_name = None

    @property
    def deployment_obj(self):
        return {
            'kind': 'Deployment',
            'apiVersion': 'extensions/v1beta1',
            'metadata': {
                'name': self.name,
                'creationTimestamp': None,
                'labels': {
                    'run': self.name,
                    **self.extra_labels
                },
                'annotations': self.annotations
            },
            'spec': {
                'replicas': 1,
                'selector': {
                    'matchLabels': {
                        'run': self.name
                    }
                },
                'template': {
                    'metadata': {
                        'creationTimestamp': None,
                        'labels': {
                            'run': self.name
                        }
                    },
                    'spec': {
                        'containers': [
                            {
                                'name': self.name,
                                'image': self.image_name,
                                'imagePullPolicy': 'IfNotPresent',
                                'ports': [
                                    {
                                        'containerPort': self.service_port
                                    }
                                ],
                                'resources': {}
                            }
                        ],
                        'imagePullSecrets': self.image_pull_secrets,
                    }
                },
                'strategy': {}
            },
            'status': {}
        }

    @property
    def image_pull_secrets(self):
        return [{'name': self.secret_name}] if self.secret_name else []

    @property
    def service_obj(self):
        return {
            'kind': 'Service',
            'apiVersion': 'v1',
            'metadata': {
                'name': self.name,
                'labels': {
                    'run': self.name,
                    **self.extra_labels
                },
                'annotations': self.annotations
            },
            'spec': {
                'ports': [
                    {
                        'protocol': 'TCP',
                        'port': self.port,
                        'targetPort': self.service_port
                    }
                ],
                'selector': {
                    'run': self.name
                }
            },
            'status': {
                'loadBalancer': {}
            }
        }

    @property
    def deployment(self):
        return Deployment(self.api, self.deployment_obj)

    @property
    def service(self):
        return Service(self.api, self.service_obj)

    @property
    def pod(self):
        pod_obj = Pod.objects(self.api).filter(selector=f'run={self.name}').response['items'][0]
        return Pod(self.api, pod_obj)

    @property
    def pod_status(self):
        try:
            return self.pod.obj['status']['phase']
        except IndexError:
            return None

    @property
    def replica_set(self):
        rs_obj = ReplicaSet.objects(self.api).filter(selector=f'run={self.name}').response['items'][0]
        return ReplicaSet(self.api, rs_obj)

    @classmethod
    def get_services(cls):
        api = cls.api
        return Service.objects(api).response['items']

    def create(
            self,
            image_name=None,
            service_port=None,
            port=None,
            extra_labels=None,
            annotations=None,
            secret_name=None
    ):
        if port is None:
            port = service_port

        if extra_labels is None:
            extra_labels = {}

        if annotations is None:
            annotations = {}

        self.image_name = image_name
        self.service_port = service_port
        self.port = port
        self.extra_labels = extra_labels
        self.annotations = annotations
        self.secret_name = secret_name

        try:
            logger.info(f'Creating deployment {self.name}')
            self.deployment.create()
            logger.info(f'Creating service {self.name}')
            self.service.create()
        except requests.exceptions.ConnectionError:
            raise KubeConnectionError('Cannot connect to cluster')

        logger.info('Waiting for pod to start running')

        timeout = 0
        while self.pod_status != 'Running' and timeout < 40:

            sleep(0.5)
            timeout += 0.5

        pod_status = self.pod_status
        if pod_status != 'Running':
            self.delete()
            raise KubeException(f'Cannot start pod: {pod_status}')

    def delete(self):
        self.service.delete()
        self.deployment.delete()
        self.replica_set.delete()
        self.pod.delete()

    def exists(self):
        return self.service.exists()
