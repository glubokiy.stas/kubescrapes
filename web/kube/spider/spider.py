import logging
from shlex import split
from typing import List

import requests
from pykube import Job, Pod, CronJob

from kube.base.object import KubeObject
from kube.exceptions import KubeConnectionError

logger = logging.getLogger(__name__)


class KubeSpider(KubeObject):

    def __init__(
            self,
            name: str,
            job_name: str,
            image_name: str,
            env: List = None,
            restart_policy: str = 'Never',
            image_pull_policy: str = 'Always',
            registry_secret_name: str = None,
            backoff_limit: int = 1,
            command: str = None,
            schedule: str = None,
            concurrency_policy: str = 'Forbid',
            success_history_limit: int = 3,
            fail_history_limit: int = 5,
            *args,
            **kwargs):
        """
        Create a spider object.
        If schedule is not defined, Job is created. Otherwise, CronJob is created.

        Args:
            name: name of the spider
            job_name: name of the Job or CronJob
            image_name: name of the image to be used
            env: array of environment variables
            restart_policy: 'Never' or 'OnFailure'
            image_pull_policy: 'Always', 'IfNotPresent' or 'Never'
            registry_secret_name: list of k8s secrets
            backoff_limit: number of failed pods to consider job failed
            command: command to run
            schedule: cron schedule, when running cronjob
            concurrency_policy: 'Allow' or 'Forbid'
            success_history_limit: how many completed jobs should be kept
            fail_history_limit: how many failed jobs should be kept
        """
        super().__init__(*args, **kwargs)

        self.name = name
        self.job_name = job_name

        self.image_name = image_name
        self.env = env or []
        self.restart_policy = restart_policy
        self.image_pull_policy = image_pull_policy
        self.registry_secret_name = registry_secret_name
        self.backoff_limit = backoff_limit
        self.command = command
        self.schedule = schedule
        self.success_history_limit = success_history_limit
        self.fail_history_limit = fail_history_limit
        self.concurrency_policy = concurrency_policy

    @property
    def job_spec(self):
        spec = {
            'template': {
                'metadata': {
                    'labels': {
                        'spider': self.name,
                        'kubescrapes': 'true'
                    }
                },
                'spec': {
                    'containers': [
                        {
                            'name': self.name,
                            'image': self.image_name,
                            'imagePullPolicy': self.image_pull_policy,
                            'env': self.env
                        }
                    ],
                    'restartPolicy': self.restart_policy,
                    'imagePullSecrets': self.image_pull_secrets
                }
            },
            'backoffLimit': self.backoff_limit
        }

        if self.command is not None:
            sh_command = split(self.command)
            spec['template']['spec']['containers'][0]['command'] = sh_command

        return spec

    @property
    def job_obj(self):
        obj = {
            'apiVersion': Job.version,
            'kind': 'Job',
            'metadata': {
                'labels': {
                    'spider': self.name,
                    'kubescrapes': 'true'
                },
                'name': self.job_name,
            },
            'spec': self.job_spec
        }

        return obj

    @property
    def cronjob_obj(self):
        return {
            'apiVersion': CronJob.version,
            'kind': 'CronJob',
            'metadata': {
                'name': self.job_name,
                'labels': {
                    'spider': self.name,
                    'kubescrapes': 'true'
                }
            },
            'spec': {
                'schedule': self.schedule,
                'concurrencyPolicy': self.concurrency_policy,
                'successfulJobsHistoryLimit': self.success_history_limit,
                'failedJobsHistoryLimit': self.fail_history_limit,
                'jobTemplate': {
                    'metadata': {
                        'labels': {
                            'spider': self.name,
                            'kubescrapes': 'true',
                            'from_cron': self.job_name
                        }
                    },
                    'spec': self.job_spec
                }
            }
        }

    @property
    def image_pull_secrets(self):
        return [{'name': self.registry_secret_name}] if self.registry_secret_name else []

    @property
    def job(self) -> Job:
        return Job(self.api, self.job_obj)

    @property
    def cronjob(self) -> CronJob:
        return CronJob(self.api, self.cronjob_obj)

    def create(self):
        """Create a spider job."""
        try:
            if self.schedule is None:
                self.job.create()
            else:
                self.cronjob.create()
        except requests.exceptions.ConnectionError:
            raise KubeConnectionError('Cannot connect to cluster')

    @classmethod
    def get_jobs(cls, selector):
        return Job.objects(cls.api).filter(selector=selector)

    @classmethod
    def get_cronjobs(cls, selector):
        return CronJob.objects(cls.api).filter(selector=selector)

    @classmethod
    def get_job(cls, **kwargs):
        return Job.objects(cls.api).get(**kwargs)

    @classmethod
    def get_cronjob(cls, **kwargs):
        return CronJob.objects(cls.api).get(**kwargs)

    @classmethod
    def get_pods(cls, selector):
        return Pod.objects(cls.api).filter(selector=selector)

    @classmethod
    def get_pod(cls, **kwargs):
        return Pod.objects(cls.api).get(**kwargs)
