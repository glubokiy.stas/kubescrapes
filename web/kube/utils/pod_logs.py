def pod_logs(pod, *args, **kwargs):
    return pod.logs(*args, **kwargs).encode('raw_unicode_escape').decode('utf-8')
